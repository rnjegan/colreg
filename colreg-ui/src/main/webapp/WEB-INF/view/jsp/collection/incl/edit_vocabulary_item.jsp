<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>


<s:bind path="${vocabularyIdentifier}[${currIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex">
			<div class="editor-list-input">
				<span class="attribute-name-helper">${vocabularyIdentifier}</span>
				<select id="${vocabularyIdentifier}${currIndex}" name="${vocabularyIdentifier}[${currIndex}]" class="form-control editor-list-input">
					<option value="" disabled selected hidden></option>
					<c:forEach items="${availableVocabularyItems}" var="availableVocabularyItem">
						<option value="${availableVocabularyItem.identifier}" 
								${availableVocabularyItem.identifier==vocabularyModelItems[currIndex] ? "selected='selected'" : ""}
								${availableVocabularyItem.deleted ? "hidden" : ""}>
							${availableVocabularyItem.displayLabel} 
							<c:if test="${availableVocabularyItem.deleted}">[<s:message code="~eu.dariah.de.colreg.common.labels.deleted" />]</c:if>
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="editor-list-item-buttons">
				<button onclick="editor.lists['${vocabularyIdentifier}'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.lists['${vocabularyIdentifier}'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.lists['${vocabularyIdentifier}'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</div>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="${vocabularyIdentifier}[${currIndex}]" /></div>
	</li>
</s:bind>