<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>

<s:bind path="spatials[${currIndex}]">
	<li class="editor-list-item mb-2 form-item${status.error ? ' has-error' : ' '}">
		<div class="d-flex">
			<span class="attribute-name-helper">spatials{}</span>
			<input type="text" class="editor-list-input form-control" id="spatials${currIndex}" name="spatials[${currIndex}]" 
				value="<c:if test="${currSpat!=null}">${currSpat}</c:if>">
			<div class="editor-list-item-buttons">
				<button onclick="editor.lists['spatials'].pushEntryUp(this); return false;" class="btn btn-xs btn-link btn-push-up"><i class="fas fa-long-arrow-alt-up"></i></button>
				<button onclick="editor.lists['spatials'].pushEntryDown(this); return false;" class="btn btn-xs btn-link btn-push-down"><i class="fas fa-long-arrow-alt-down"></i></button>
				<button onclick="editor.lists['spatials'].removeEntry(this); return false;" class="btn btn-xs btn-link"><i class="fas fa-times"></i></button>
			</div>
		</div>
		<div class="col-sm-9 offset-sm-3"><sf:errors element="div" cssClass="validation-error alert alert-danger" path="spatials[${currIndex}]" /></div>
	</li>
</s:bind>