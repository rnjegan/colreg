<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>


<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div class="col-12">
	    	<h1>${vocabulary.localizedLabel}</h1>
	    	<input type="hidden" id="vocabulary-id" value="${vocabulary.id}" />
		    <div id="vocabulary-table-container" class="col-12">
			    <div class="row">
			    	<div class="col-12 col-md-8">
			    		<div class="data-table-count float-left">
							<label><s:message code="~eu.dariah.de.colreg.common.labels.show" />: 
								<select class="custom-select custom-select-sm form-control form-control-sm" aria-controls="vocabulary-table">
								  <option>10</option>
								  <option>25</option>
								  <option>50</option>
								  <option>100</option>
								  <option><s:message code="~eu.dariah.de.colreg.common.labels.all" /></option>
								</select>
							</label>
						</div>	
			    		<div class="data-table-filter float-left">
			    			<label><s:message code="~eu.dariah.de.colreg.common.labels.filter" />: 
								<input type="text" class="form-control form-control-sm" aria-controls="vocabulary-table">
							</label>
						</div>
	   				</div>
	   				<div class="col-12 col-md-4">
	   					<div style="text-align: right;">
	   						<c:if test="${_auth!=null && _auth.auth}">
								<button onclick="vocabularyTable.triggerEditVocabularyItem('new');" class="btn btn-primary">
									<i class="fas fa-plus-circle"></i> <s:message code="~eu.dariah.de.colreg.view.agent.actions.add_vocabulary_item" />
								</button>								
								<input type="hidden" id="table-edit-mode" value="true" />
							</c:if>
	   					</div>
	   				</div>
	 			</div>
	 			<div class="row">
	 				<div class="col-12">
					    <table id="vocabulary-table" class="table table-striped table-bordered" style="width:100%" role="grid">
							<thead>
								<tr>
									<th><s:message code="~eu.dariah.de.colreg.model.vocabulary_item.identifier" /></th>
									<th><s:message code="~eu.dariah.de.colreg.model.vocabulary_item.default_name" /></th>
									<th><s:message code="~eu.dariah.de.colreg.model.vocabulary_item.local_name" /></th>
									<c:if test="${_auth!=null && _auth.auth}">
										<th><!-- Actions --></th>
									</c:if>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="${_auth!=null && _auth.auth ? '4' : '3'}" align="center"><s:message code="~eu.dariah.de.colreg.common.view.notifications.nothing_fetched_yet" /></td>
							</tr>
							</tbody>
						</table>
					</div>
					<div class="col-12">
						<div class="alert alert-info alert-sm pull-right"><s:message code="~eu.dariah.de.colreg.view.vocabulary_item.labels.hint_translations" /></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>