<%@ page language="java" contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div class="col-12">
	    	<h1><s:message code="~eu.dariah.de.colreg.common.exception.not_found.title" /></h1>
	    	<p>
				<s:message code="~eu.dariah.de.colreg.common.exception.not_found.message" arguments="${url}" />
				<ul>
					<li><a href="<s:url value='/' />"><s:message code="~eu.dariah.de.colreg.view.common.labels.dashboard" /></a></li>
	    			<c:if test="${_draftCount > 0}">
	    				<li><a href="<s:url value='/drafts/' />"><s:message code="~eu.dariah.de.colreg.view.common.labels.drafts" arguments="${_draftCount}" /></a></li>
	    			</c:if>
	    			<li><a href="<s:url value='/collections/' />"><s:message code="~eu.dariah.de.colreg.titles.collections" /></a></li>
	    			<li><a href="<s:url value='/agents/' />"><s:message code="~eu.dariah.de.colreg.titles.agents" /></a></li>
				</ul>
			</p>
		</div>
	</div>
</div>
