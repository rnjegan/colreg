<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div id="notifications-area" class="col-10 offset-1"></div>
	    <div class="col-12">
	    	<h1><s:message code="~eu.dariah.de.minfba.common.user_profile" /></h1>
		    <c:choose>
				<c:when test="${_auth!=null && _auth.auth==true}">
					<form>
						<div class="form-group">
						    <label for="authorizedAs"><s:message code="~eu.dariah.de.minfba.common.user_profile.autorized_as" /></label>
						    <input type="text" readonly class="form-control-plaintext" id="authorizedAs" value="${_auth.displayName}">
						</div>
					</form>
				</c:when>
				<c:otherwise>
					<p><s:message code="~eu.dariah.de.minfba.common.user_profile.not_logged_in" /></p>
				</c:otherwise>
			</c:choose>
		</div>
	</div>
</div>