<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>

<tiles:importAttribute name="fluidLayout" />

<div class="container<c:if test="${fluidLayout==true}">-fluid</c:if>">
	<div class="row">
	    <div class="col-12">
	    	<h1><s:message code="~eu.dariah.de.colreg.titles.agents" /></h1>
		    <div id="agent-table-container" class="col-12">
			    <div class="row">
			    	<div class="col-sm-12 col-md-8">
			    		<div class="data-table-count float-left">
							<label><s:message code="~eu.dariah.de.colreg.common.labels.show" />: 
								<select class="custom-select custom-select-sm form-control form-control-sm" aria-controls="agent-table">
								  <option>10</option>
								  <option>25</option>
								  <option>50</option>
								  <option>100</option>
								  <option><s:message code="~eu.dariah.de.colreg.common.labels.all" /></option>
								</select>
							</label>
						</div>	
			    		<div class="data-table-filter float-left">
			    			<label><s:message code="~eu.dariah.de.colreg.common.labels.filter" />: 
								<input type="text" class="form-control form-control-sm" aria-controls="agent-table">
							</label>
						</div>
	   				</div>
	   				<div class="col-sm-12 col-md-4">
	   					<div style="text-align: right;">
		   					<a id="btn-add-agent" href="<s:url value='new' />" class="btn btn-primary">
								<i class="fas fa-plus-circle"></i> <s:message code="~eu.dariah.de.colreg.titles.new_agent" />
							</a>
	   					</div>
	   				</div>
	 			</div>
	 			<div class="row">
	 				<div class="col-sm-12">
					    <table id="agent-table" class="table table-striped table-bordered" style="width:100%" role="grid">
					    	<thead>
								<tr>
									<th></th> 
									<th><s:message code="~eu.dariah.de.colreg.common.labels.image" /></th>
									<th><s:message code="~eu.dariah.de.colreg.model.agent.name" /></th>
									<th><s:message code="~eu.dariah.de.colreg.model.agent.type" /></th>
									<th><s:message code="~eu.dariah.de.colreg.model.agent.current_version" /></th>
								</tr>
							</thead>
							<tbody>
							<tr>
								<td colspan="5" align="center"><s:message code="~eu.dariah.de.colreg.common.view.notifications.nothing_fetched_yet" /></td>
							</tr>
							</tbody>
					    </table>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>