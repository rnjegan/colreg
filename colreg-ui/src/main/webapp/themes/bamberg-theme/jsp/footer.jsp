<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<footer class="footer">
	<div class="container-fluid" style="max-width: 1200px">

		<div class="row">
		
		
		<div class="footer-left">
			<div>
				<p> 
					<s:message code="~de.unibamberg.minf.common.labels.license">
						<s:argument><a href="https://creativecommons.org/licenses/by/4.0/"><s:message code="~de.unibamberg.minf.common.labels.license.ccby40" /></a></s:argument>
					</s:message>
				</p>
			</div>
			<p>
				<a href="https://www.uni-bamberg.de/">
					Otto-Friedrich-Universit&auml;t Bamberg
					<br>
				</a>
			</p>
		</div>
		<div class="footer-right"
		</div>
		
		
		</div>
	</div>

</footer>
