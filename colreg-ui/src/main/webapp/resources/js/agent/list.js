var agentTable;
$(document).ready(function() {
	agentTable = new AgentTable();
});

var AgentTable = function() {
	this.prepareTranslations(["~eu.dariah.de.colreg.common.labels.deleted",
	                          "~eu.dariah.de.colreg.common.labels.draft",
	                          "~eu.dariah.de.colreg.common.labels.published",
	                          "~eu.dariah.de.colreg.common.labels.no_image",
	                          "~eu.dariah.de.colreg.common.labels.valid"]);
	this.createTable();
};

AgentTable.prototype = new BaseTable(__util.composeUrl("agents/list"), "#agent-table-container");

AgentTable.prototype.createTable = function() {
	var _this = this;
	this._base.table = $('#agent-table').DataTable($.extend(true, {
		"responsive": true,
		"order": [[2, "asc"]],
		"columnDefs": [
	       {
	           "targets": [0],
	           "class" : "no-wrap",
	           "data": function (row, type, val, meta) { return agentTable.renderBadgeColumn(row, type, val, meta); }
	       }, {
	           "targets": [1],
	           "data": function (row, type, val, meta) { return _this.renderImageColumn(row, type, val, meta); },
	           "responsivePriority": 10001,
	           "class" : "td-center td-image"
	       }, {	
	    	   "targets": [2],
	    	   "data": function (row, type, val, meta) { return _this.getRowLink(row) + row.entity.name + "</a>"; },
	    	   //"width" : "100%"
	       }, {	
	    	   "targets": [3],
	    	   "data": function (row, type, val, meta) { return  _this.getRowLink(row) + row.entity.type + "</a>"; },
	    	   "class" : "no-wrap"
	       }, {	
	    	   "targets": [4],
	    	   "data": function (row, type, val, meta) { return agentTable.renderVersionColumn(row, type, val, meta); }
	    	  /* "visible" : false*/
	       }	       
	   ]
	}, this.baseSettings));

	$("#btn-add-agent").on("click", function(e) {
		if (!__util.isLoggedIn()) {
			__util.showLoginNote();
			e.preventDefault();
			return false;
		}
		return true;
	});
};

AgentTable.prototype.renderImageColumn = function(row, type, val, meta) {
	if (row.entity.primaryImage!==undefined && row.entity.primaryImage!==null) {
		return this.getRowLink(row) + "<img class='list-thumb' src=\"" + row.entity.primaryImage.thumbnailUrl + "\" /></a>";
	} else {
		return "<div class=\"list-thumb-nopreview\">" + 
						"<span>" + this.getRowLink(row) + __translator.translate("~eu.dariah.de.colreg.common.labels.no_image") + "</a></span>" +
			   "</div>"
	}
};

AgentTable.prototype.renderVersionColumn = function(row, type, val, meta) {
	if (type=="display") {
		return this.getRowLink(row) + row.entity.displayTimestamp + "</a>";
	} else {
		return row.entity.timestamp;
	}
	return "";
};

AgentTable.prototype.renderBadgeColumn = function(row, type, val, meta) {
	var result = "";
	if (type=="display") {
		result += this.getRowLink(row);
		if (row.entity.deleted) {
			result += '<span class="badge badge-danger">' + __translator.translate("~eu.dariah.de.colreg.common.labels.deleted") + '</span> ';
		} else {
			result += '<span class="badge badge-primary">' + __translator.translate("~eu.dariah.de.colreg.common.labels.valid") + '</span> ';
		} 
		result += "</a>";
	} else {
		if (row.entity.deleted) {
			result += __translator.translate("~eu.dariah.de.colreg.common.labels.deleted");
		} else {
			result += __translator.translate("~eu.dariah.de.colreg.common.labels.valid");
		} 
	}
	return result;
};

AgentTable.prototype.getRowLink = function(row) {
	return "<a href=\"" + __util.composeUrl("agents/" + row.entity.id) + "\">";
};