.. DARIAH-DE Collection Registry documentation master file

DARIAH-DE Collection Registry
=============================

Die DARIAH-DE Collection Registry

Einführung
===========

Die `DARIAH-DE Collection Registry <https://colreg.de.dariah.eu>`_ ist ein von DARIAH-DE bereitgestelltes, kostenfreies Tool, um Sammlungen zu beschreiben. Das bedeutet, dass an dieser Stelle Referenzen auf Sammlungsdaten beschrieben und gespeichert (und – soweit vorhanden – eine Zugriffsmethode auf die Daten angegeben) werden und nicht die Daten von Sammlungen selbst. Sie beschreiben also in der Collection Registry Ihre Sammlung – samt technischen Schnittstellen, und Sie können dafür auf ein weit umfangreicheres Beschreibungsschema (`DARIAH Collection Description Data Model – DCDDM <https://github.com/DARIAH-DE/DCDDM>`_) zugreifen als dies bei der Veröffentlichung mit z. B. `Dublin Core Simple <http://dublincore.org/documents/dces/>`_ möglich ist.

Die Collection Registry ist verankert in der DARIAH-DE `Data Federation Architecture <https://de.dariah.eu/data-federation-architecture>`_ und spielt dort eine wichtige Rolle im Forschungsdatenzyklus und im Forschungsdatenmanagement. Weiterführende Empfehlungen zum Forschungsdatenmanagement finden Sie auf dem  `DARIAH-DE Portal <https://de.dariah.eu/>`_.

Die wissenschaftliche Sammlung 
===============================

Als wissenschaftliche Sammlung werden in DARIAH-DE keine lediglich nach formalen Kriterien aufgebauten Digitalisat- oder Volltextsammlungen verstanden (siehe `Report zu Sammlungskonzepten aus DARIAH-DE II <https://wiki.de.dariah.eu/download/attachments/26150061/R4.2.3-final.pdf?version=2&modificationDate=1450260854053&api=v2>`_ ), sondern Sammlungen, die nach inhaltlichen, forschungsfragenbezogenen oder fachlichen Kriterien gebildet wurden. Diese sind dabei möglicherweise aus formalen Sammlungen gebildet worden, können aber auch aus mehreren anderen solcher formalen Sammlungen zusammengefasst worden sein. In diesem Sinne sind formale Sammlungen Rohdaten und Grundlage für die Erstellung von wissenschaftlichen Sammlungen. Diese sind darüber hinaus Aggregationen von Forschungs- und Metadaten, die von WissenschaftlerInnen zur Beantwortung von Forschungsfragen innerhalb bestimmter Forschungskontexte zusammengestellt und mit weiteren Informationen wie Normdaten oder Annotationen maschinenlesbar angereichert werden. 

Eine Sammlung wissenschaftlich relevanter Forschungsdaten (siehe `Definition nach Oltersdorf/Schmunk 2016 <https://www.degruyter.com/view/j/bfup.2016.40.issue-2/bfp-2016-0036/bfp-2016-0036.xml?format=INT>`_) 

* ist Gegenstand wissenschaftlicher Fragestellungen und dient der Validierung von Aussagen, Methoden, Thesen, Hypothesen oder Theorien in Forschung und Lehre
* kann Ursprung und Ergebnis wissenschaftlicher Arbeit sein (research data life cycle)
* ist in einer regelhaften Form dokumentiert, idealerweise nach internationalen Standards erfasst und mit Normdaten ausgezeichnet
* gibt Auskunft über ihre Nutzungsbedingungen (Lizenzen)
* dient der Ordnung der Sammlungsgegenstände und der archivischen Sicherung 

Letztlich können demnach aus Sicht von DARIAH-DE nahezu alle Objekte zu einer Sammlung aggregiert werden. Es muss sich dabei nicht um physische Objekte oder deren Abbildung handeln, sondern es können auch beispielsweise Metadaten zu Objekten sein oder graphische Auswertungen oder Visualisierungen von Datenmengen.

Die Begriffe Kollektion und Sammlung haben innerhalb von DARIAH-DE prinzipiell dieselbe Bedeutung, Kollektion wird jedoch als Übersetzung von Collection ebenfalls genutzt, z. B. im Publikator oder im `DARIAH-DE Repository <https://de.dariah.eu/repository>`_. Eine Kollektion bezeichnet hier genauso eine Menge an Forschungsdaten, praktisch gesehen meist eine Menge an Dateien. 

Der Nutzen der Collection Registry
===================================

…für existierende Sammlungen, mit oder ohne technische Schnittstelle zu den (digitalen) Objekten
*****************************************************************************************************

Sind die Dateien einer Sammlung bereits öffentlich zugänglich und sind idealerweise schon mit persistenten Identifikatoren versehen und es kümmert sich eine Gedächtnisorganisation um deren sichere Aufbewahrung, dann können diese Dateien guten Gewissens als Sammlung in der DARIAH-DE Collection Registry eintragen und dort beschrieben werden. Existiert eine technische Schnittstelle zu den Objekten der Sammlung, kann auch diese in der Sammlungsbeschreibung angeben werden. So werden die Inhalte der Kollektion in der `Generischen Suche <https://search.de.dariah.eu>`_ von DARIAH-DE indiziert und sind dort auffindbar.

…für Sammlungen, die noch nicht öffentlich verfügbar sind und die veröffentlicht werden sollen
*************************************************************************************************

Existieren die Forschungsdaten als eine Sammlung oder auch als Einzeldateien nur lokal auf einer Festplatte, auf einer CD oder an einer anderen nicht öffentlich zugänglichen Stelle, dann können die Daten nicht von anderen interessierten Forscherinnen und Forschern recherchiert und gefunden werden. Die Forschungsdaten solcher Sammlungen – sollten sie nicht gepflegt und kuratiert werden –, stehen der Wissenschaft für die Nachnutzung also gar nicht oder nicht nachhaltig zur Verfügung. Wenn solche Forschungsergebnisse sicher und zitierbar aufbewahrt werden sollen, können sie über den `DARIAH-DE Publikator <https://repository.de.dariah.eu/publikator/>`_ in das DARIAH-DE Repositorium eingespielt werden. Dann…

* werden die Forschungsdaten sicher im Repository gespeichert,
* bekommen die Forschungsdaten einen Persistenten Identifikator (die Kollektion selbst und sämtliche beinhalteten Dateien),
* sind so dauerhaft referenzierbar und zitierbar,
* können in der Collection Registry als Kollektion beschrieben werden und
* sind dann in der Generischen Suche recherchierbar.

Die Forschungsdaten sind damit in den Forschungsdatenzyklus aufgenommen und stehen so für die Nachnutzung zur Verfügung.

Wo finde ich die Collection Registry?
======================================

Sie erreichen die DARIAH-DE Collection Registry im DARIAH-DE Portal von der Seite der Tools und Dienste aus oder auch direkt über diesen
`Link zur DARIAH-DE Collection Registry <http://colreg.de.dariah.eu>`_

Um den Dienst nutzen zu können, melden Sie sich bitte mit Ihrem DARIAH- oder Föderations-Account an. Sollten Sie keinen DARIAH-Account haben, können Sie `HIER <https://auth.dariah.eu/cgi-bin/selfservice/ldapportal.pl?mode=selfreg>`_ einen solchen beantragen.

Wie benutze ich die Collection Registry
========================================

Wie melde ich mich an?
***********************

Haben Sie die Seite zur Collection Registry aufgerufen, erscheint zunächst die Startseite. Hier sehen Sie auf der linken Seite ein Dashboard mit den neuesten Einträgen und Veränderungen bzw. letzten Aktivitäten. Neben dem Dashboard sehen sie ein Objektnetzwerk bestehend aus sämtlichen Sammlungsbeschreibungen und Akteuren zur visuellen Aufbereitung des Inhalts der Collection Registry und um einen alternativen Zugang zu den einzelnen Objekten zu bieten. 

.. figure:: ../img/dashboard_cr.png


Wollen Sie nun selber eine Sammlung anlegen oder Ihre persönlichen Entwürfe einsehen, können Sie sich mit Ihrem DARIAH-Account anmelden. Gehen Sie dafür über den Anmelde-Button oben rechts auf der Startseite. Nach Betätigung dieses Buttons werden Sie weitergeleitet zu folgendem Fenster. Suchen Sie hier *DARIAH* und gehen dann auf *Auswählen*. 

.. figure:: ../img/anmeldung_cr_dariah.png

Danach sollte folgendes Fenster mit dem eigentlichen Log-In über die *DARIAH AAI (Authentication Authorisation Infrastructure)* erscheinen:

.. figure:: ../img/dariah_aai.png

Der Vorteil davon ist, dass Sie sich, um alle DARIAH-Dienste nutzen zu können, nur ein einziges Mal einloggen müssen. Ab diesem Zeitpunkt stehen Ihnen alle Dienste kostenfrei zur Verfügung. 

Nach der Anmeldung
*******************
Nachdem Sie sich angemeldet haben, sollten Sie oben rechts Ihren DARIAH-Nutzernamen sehen und sich dort auch wieder abmelden können. Sie sind nun berechtigt, Sammlungsbeschreibungen anzulegen und zu bearbeiten. Zudem können Sie Ihre Entwürfe einsehen und weiter ergänzen. Wie eine neue Sammlungsbeschreibung angelegt wird und wo man die eigenen Entwürfe findet, die vom DARIAH-DE Publikator automatisch angelegt wurden, erfahren Sie im nächsten Kapitel.

Das Anlegen einer neuen Sammlungsbeschreibung und das Bearbeiten von Entwürfen
************************************************************************************
Wenn Sie eine neue Sammlungsbeschreibung erstellen wollen, müssen Sie zunächst das Dashboard verlassen und *Zu den Sammlungen* gehen.  

.. figure:: ../img/zu_den_sammlungen.png

Hier haben Sie Einsicht in sämtliche veröffentlichte Sammlungsbeschreibungen und in Ihre persönlichen Entwürfe. Bereits veröffentliche Sammlungsbeschreibungen können ebenfalls bearbeitet werden.


Möchten Sie nun eine neue Sammlung anlegen, klicken Sie oben rechts auf:
 

.. figure:: ../img/sammlung_hinzufügen.png

Danach öffnet sich ein Fenster mit dem Sammlungseditor, über den Sie Ihre Beschreibung bequem anlegen können. Sie haben nun den Entwurf einer neuen Sammlung angelegt.

.. figure:: ../img/sammlung_entwurf.png

Innerhalb des Sammlungseditors haben Sie nun die Möglichkeit, Ihre Kollektion ausführlich und granular zu beschreiben. Diese in Eigenschaften unterteilten Kategorien finden Sie auf der linken Seite des Sammlungseditors:

.. figure:: ../img/beschreibungen.png

Wichtig zu beachten sind für Sie zunächst einmal die Obligatorischen Beschreibungen. Hierbei handelt es sich z.T. um Pflichtfelder, die ausgefüllt sein müssen. Sie umfassen die folgenden Angaben:

* Die Sprache (Achtung! Hierbei handelt es sich um die Sprache des Beschreibungssets, nicht der Sammlungsobjekte)
* Der Titel der Sammlung
* Falls vorhanden: das Acronym
* Eine kurze Beschreibung
* Der Kollektionstyp gibt an, ob es sich z. B. um eine digitale Sammlung handelt
* Rechte an der Kollektionsbeschreibung
* Zugriffsrechte auf die Inhalte der Sammlungsbeschreibung

.. note:: **Achtung!** Kollektionsbeschreibungen und Inhalte einer Sammlung unterliegen unterschiedlichen Lizenzbestimmungen. Wenn Sie mehr zum Thema Lizenzen erfahren möchten, können Sie sich z. B. hier informieren.

Im ersten Abschnitt werden Titel, Acronym (Kurzwort aus Anfangsbuchstaben der Sammlung, z. B. Bayrisches Digitales Repositorium (BDR )) und Sprache der Sammlungsbeschreibung angegeben. Möchten Sie eine weitere zusätzliche Beschreibung ergänzen, bspw. in einer anderen Sprache, gehen Sie auf *Beschreibung hinzufügen*. Ein Beispiel für das Anlegen mehrerer Beschreibungen finden Sie `hier. <https://colreg.de.dariah.eu/colreg-ui/collections/57a1df1d2d46f3061eb11e80>`_

.. figure:: ../img/obligatorische_beschreibungen_1.png



Sollten Sie nicht sicher sein, wie Sie bestimmte Felder ausfüllen sollen, können Sie unsere Editor-Option oben rechts einblenden.

.. figure:: ../img/editor_optionen.png

Daraufhin sehen Sie zu jedem Feld eine kurze Beschreibung und Erwartungen, wie diese ausgefüllt werden müssen bzw. sollten.

Für die ersten Beschreibungs-Felder sähe dies so aus:

.. figure:: ../img/beschreibungen_editor_optionen.png


Das Anlegen eines neues Akteurs
**********************************
Wenn Sie eine neue Sammlungsbeschreibung anlegen, sind Sie verpflichtet, einen Akteur zu der enstprechenden Sammlung zu benennen. Sie können aus einer Liste bereits angelegter Akteure auswählen. Wenn es den Akteur noch nicht gibt, müssen Sie ihn neu anlegen.

Ein Akteur kann eine einzelne Person sein oder auch eine Organisation. Um eine neuen Akteur hinzuzufügen, gehen Sie zunächst *Zu den Akteuren*. Sind Sie bereits in der Collection Registry angemeldet erscheint folgendes Fenster:

.. figure:: ../img/akteure.png

Klicken Sie oben rechts auf *Neuer Akteur* und es erscheint der Akteur-Editor mit diversen Beschreibungsmöglichkeiten:

.. figure:: ../img/neuer_akteur.png

Zwingend ausgefüllt sein müssen die als solche markierten Pflichtfelder. Sind Sie mit den Eintragungen fertig, klicken Sie auf Speichern und der neu angelegte Akteur ist nun im Sammlungseditor auffindbar und nutzbar.

Das Speichern einer neuen Sammlungsbeschreibung
*************************************************

Um Ihre Sammlungsbeschreibung zu speichern, klicken Sie einfach auf den entsprechenden Button. Danach erscheint folgende Meldung:

.. figure:: ../img/sammlung_speichern.png

Ihre Sammlungsbeschreibung ist nun als Entwurf für Sie gespeichert und auch nur für Sie einsehbar und bearbeitbar. Sie können jederzeit daran arbeiten und sie auch wieder löschen.

.. figure:: ../img/sammlung_speichern_entwurf.png


Das Veröffentlichen einer Sammlungsbeschreibung 
***************************************************

Sind Sie mit ihrer Sammlungsbeschreibung zufrieden und möchten, dass sie öffentlich einsehbar ist, können Sie sie in der Collection Registry veröffentlichen. Dazu klicken Sie einfach auf:

.. figure:: ../img/speichern_und_veröffentlichen.png

Danach ist Ihr Entwurf öffentlich einsehbar und auch der Status der Beschreibung hat sich geändert. Sämtliche Nutzer der Collection Registry können sie einsehen und Änderungen sind ebenfalls extern sichtbar. Haben Sie einen technischen Zugang zu Ihrer Kollektion angegeben, werden die Objekte der Kollektion von der Generischen Suche indiziert und sind so dort findbar.
Sie haben weiterhin Administrationsrechte auf die Sammlungsbeschreibung. Dieses Feld finden Sie ganz unten im Beschreibungseditor. Hier finden Sie die ID der Beschreibung, eine Versions-ID, den Status der aktuellen Version und wann die Beschreibung initiativ angelegt wurde.




Best Practice Empfehlungen
============================

.. figure:: ../img/4Schritte.png
 


